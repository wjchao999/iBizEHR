export default {
  fields: {
    dcwjtm: '调查问卷题目',
    createman: '建立人',
    bz: '备注',
    updateman: '更新人',
    trmtrainactmentid: '培训活动评估标识',
    updatedate: '更新时间',
    createdate: '建立时间',
    enable: '逻辑有效标志',
    pxjg: '评估结果',
    trmtrainactmentname: '培训活动评估名称',
    trmtrainactapplyname: '培训活动申请名称',
    trmtrainactapplyid: '培训活动申请标识',
  },
};