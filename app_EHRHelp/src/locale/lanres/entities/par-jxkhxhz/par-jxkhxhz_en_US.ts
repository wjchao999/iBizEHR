
export default {
  fields: {
    xh: '排序号',
    updatedate: '更新时间',
    jswmjc: '月度考核',
    szmzpy: '集中述职评审',
    khlx: '考核对象',
    ywnlks: '工作周报考核',
    parjxkhxhzname: '绩效考核项得分汇总名称',
    createman: '建立人',
    zz: '所属组织',
    ygid: '员工ID',
    gznljtd: '工作能力及态度评价（上半年）',
    updateman: '更新人',
    gznljtd_xbn: '工作能力及态度评价（下半年）',
    createdate: '建立时间',
    parjxkhxhzid: '绩效考核项得分汇总标识',
    gzjx: '年度量化目标（工作述职）',
    yg: '员工',
    bm: '部门',
  },
};