export default {
  fields: {
    szqz: '考核权重（%）',
    parkhzcmxid: '考核内容标识',
    createman: '建立人',
    updateman: '更新人',
    sfqy: '是否启用',
    parkhzcmxname: '考核内容',
    createdate: '建立时间',
    updatedate: '更新时间',
    khdx: '考核对象',
    parjxkhjcszname: '考核方案',
    parjxkhjcszid: '考核方案ID',
  },
};