import { Http,Util } from '@/utils';
import ParJxkhxhzServiceBase from './par-jxkhxhz-service-base';


/**
 * 考核内容评分汇总服务对象
 *
 * @export
 * @class ParJxkhxhzService
 * @extends {ParJxkhxhzServiceBase}
 */
export default class ParJxkhxhzService extends ParJxkhxhzServiceBase {

    /**
     * Creates an instance of  ParJxkhxhzService.
     * 
     * @param {*} [opts={}]
     * @memberof  ParJxkhxhzService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}