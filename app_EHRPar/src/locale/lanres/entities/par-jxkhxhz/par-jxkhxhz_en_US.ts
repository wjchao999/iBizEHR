
export default {
  fields: {
    xh: '排序号',
    updatedate: '更新时间',
    jswmjc: '月度考核',
    szmzpy: '集中述职评审',
    khlx: '考核对象',
    ywnlks: '工作周报考核',
    parjxkhxhzname: '绩效考核项得分汇总名称',
    createman: '建立人',
    zz: '所属组织',
    ygid: '员工ID',
    gznljtd: '工作能力及态度评价（上半年）',
    updateman: '更新人',
    gznljtd_xbn: '工作能力及态度评价（下半年）',
    createdate: '建立时间',
    parjxkhxhzid: '绩效考核项得分汇总标识',
    gzjx: '年度量化目标（工作述职）',
    yg: '员工',
    bm: '部门',
  },
	views: {
		ygkhxgridview: {
			caption: "员工考核内容及评分汇总",
      title: '员工考核内容及评分汇总',
		},
		gridview: {
			caption: "考核内容评分汇总",
      title: '绩效考核项得分汇总表格视图',
		},
		ygkhxeditview: {
			caption: "员工考核内容及评分汇总",
      title: '员工考核内容及评分汇总',
		},
		editview2: {
			caption: "考核内容评分汇总",
      title: '绩效考核项得分汇总编辑视图',
		},
		pickupview: {
			caption: "考核内容评分汇总",
      title: '绩效考核项得分汇总数据选择视图',
		},
		mpickupview: {
			caption: "考核内容评分汇总",
      title: '绩效考核项得分汇总数据多项选择视图',
		},
		pickupgridview: {
			caption: "考核内容评分汇总",
      title: '绩效考核项得分汇总选择表格视图',
		},
		editview: {
			caption: "考核内容评分汇总",
      title: '绩效考核项得分汇总编辑视图',
		},
	},
	ygkhx_form: {
		details: {
			group1: "考核内容评分汇总", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "绩效考核项得分汇总标识", 
			srfmajortext: "绩效考核项得分汇总名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			parjxkhxhzname: "绩效考核项得分汇总名称", 
			ygid: "员工ID", 
			xh: "排序号", 
			khlx: "考核对象", 
			yg: "员工", 
			zz: "所属组织", 
			bm: "部门", 
			gzjx: "年度量化目标（工作述职）", 
			jswmjc: "月度考核", 
			ywnlks: "工作周报考核", 
			gznljtd: "工作能力及态度评价（上半年）", 
			gznljtd_xbn: "工作能力及态度评价（下半年）", 
			parjxkhxhzid: "绩效考核项得分汇总标识", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "考核项得分汇总", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "绩效考核项得分汇总标识", 
			srfmajortext: "绩效考核项得分汇总名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			ygid: "员工ID", 
			xh: "排序号", 
			yg: "员工", 
			zz: "所属组织", 
			bm: "部门", 
			gzjx: "年度量化目标（工作述职）", 
			gznljtd: "工作能力及态度评价（上半年）", 
			ywnlks: "工作周报考核", 
			szmzpy: "集中述职评审", 
			jswmjc: "月度考核", 
			parjxkhxhzname: "绩效考核项得分汇总名称", 
			parjxkhxhzid: "绩效考核项得分汇总标识", 
		},
		uiactions: {
		},
	},
	ygkhx_grid: {
		columns: {
			xh: "排序号",
			khlx: "考核对象",
			yg: "员工",
			zz: "所属组织",
			bm: "部门",
			gzjx: "年度量化目标（工作述职）",
			jswmjc: "月度考核",
			ywnlks: "工作周报考核",
			gznljtd: "工作能力及态度评价（上半年）",
			gznljtd_xbn: "工作能力及态度评价（下半年）",
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			xh: "排序号",
			yg: "员工",
			zz: "所属组织",
			bm: "部门",
			gzjx: "年度量化目标（工作述职）",
			gznljtd: "工作能力及态度评价（上半年）",
			ywnlks: "工作周报考核",
			szmzpy: "集中述职评审",
			jswmjc: "月度考核",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	ygkhxgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem18: {
			caption: "Help",
			tip: "Help",
		},
	},
	ygkhxeditviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem18: {
			caption: "Help",
			tip: "Help",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	editview2toolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
};