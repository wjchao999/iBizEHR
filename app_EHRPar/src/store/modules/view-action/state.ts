/**
 * 所有应用视图
 */
export const viewstate: any = {
    appviews: [
        {
            viewtag: '01dbe2eb55658778f14484a5fda6fb52',
            viewmodule: 'PAR',
            viewname: 'ParTzggPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '02139952c2ac2e90cbc2d5712c5ded50',
            viewmodule: 'PAR',
            viewname: 'ParIntegralRuleEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '057fd0a13dc6d8415557b4c0c01ec673',
            viewmodule: 'PAR',
            viewname: 'ParExamCyclePickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '44b5e93634d7355506f37cde5dd7dde8',
            ],
        },
        {
            viewtag: '0587c03dd8511394456567f75983d68d',
            viewmodule: 'PAR',
            viewname: 'ParKhzcmxPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c254a4abcdeaa4de6c2969b4f4335e9e',
            ],
        },
        {
            viewtag: '05888e4abd44fea4605d6f22c87eb807',
            viewmodule: 'PAR',
            viewname: 'ParJxjgEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '11beb7ce619d4fa3c5286eba91be6d7a',
            viewmodule: 'PAR',
            viewname: 'ParJxjgJXPGDJGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'a97ed731f303a44f10abb4df79b7bfc5',
            ],
        },
        {
            viewtag: '11e69b35ff04251d7f8e7295a1d63f06',
            viewmodule: 'PAR',
            viewname: 'ParLdkhqzGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '663fc0adb5dc1b0c3d92e2bdeb30c163',
            ],
        },
        {
            viewtag: '17647fd91026d622fb7e3b49eeeea019',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbmxEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '188a0a8121edcb88a352a84cf562aa58',
            viewmodule: 'PAR',
            viewname: 'ParIntegralRuleGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '5367978cf366f005827916576e777686',
                '02139952c2ac2e90cbc2d5712c5ded50',
            ],
        },
        {
            viewtag: '1e42ac493a08f87edf0c49360e4450e3',
            viewmodule: 'PAR',
            viewname: 'ParExamCycleEditView2',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '25a6fb2dc9f85ee002bcbeca2f1a8205',
            viewmodule: 'PAR',
            viewname: 'ParExamCycleGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '963f9744f61222f20041022b8b7eba31',
            ],
        },
        {
            viewtag: '2698c6c20aa3e029be5d7b71d05d529d',
            viewmodule: 'PAR',
            viewname: 'ParKhfaGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'a0562928c523c5554de86ca7cce6cf14',
                '2cafcda71b14acdc7fbdd67a627f826f',
            ],
        },
        {
            viewtag: '2c011a5df68fea9e14fd6dd50a31b3da',
            viewmodule: 'PAR',
            viewname: 'ParJxkhxhzYGKHXGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '94611ef3a4827e95f4bea2199ed9b4a2',
            ],
        },
        {
            viewtag: '2cafcda71b14acdc7fbdd67a627f826f',
            viewmodule: 'PAR',
            viewname: 'ParKhfaEditView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '321a2c02eb80f38bde111ba6ceeda15d',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbmxKHEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '35977acfc0970b347cace0a7d8ccfac5',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbNDLHMBXZGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c088f921bed521fc40c770bfcf4b980e',
            ],
        },
        {
            viewtag: '44b5e93634d7355506f37cde5dd7dde8',
            viewmodule: 'PAR',
            viewname: 'ParExamCyclePickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '48cb9678c112e2bfeceb56c96c8c0de5',
            viewmodule: 'PAR',
            viewname: 'ParTzggMPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '01dbe2eb55658778f14484a5fda6fb52',
            ],
        },
        {
            viewtag: '4cb79515b9133499fc74b275a26e2da5',
            viewmodule: 'PAR',
            viewname: 'ParTzggGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'b7f7b92b88ba971f28bbb5aa18145e46',
                'c7741fa56792325b27e13d323a735061',
            ],
        },
        {
            viewtag: '51460be10bc6dc74ec79e53ae545e1c7',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbKHGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'e519a0448aa58cc9ef26d17863520b2b',
            ],
        },
        {
            viewtag: '5367978cf366f005827916576e777686',
            viewmodule: 'PAR',
            viewname: 'ParIntegralRuleEditView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '64e14790f02145378336091f8f95421e',
            viewmodule: 'PIM',
            viewname: 'PimPersonPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'ccbb9b1dc1b969f43ca9f0d6d6ad16b2',
            ],
        },
        {
            viewtag: '663fc0adb5dc1b0c3d92e2bdeb30c163',
            viewmodule: 'PAR',
            viewname: 'ParLdkhqzEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'e9be57ed7f3eaa058e17276a047e1d92',
            ],
        },
        {
            viewtag: '6dfdcefce006ad3505558f3babcddcde',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbmxGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '17647fd91026d622fb7e3b49eeeea019',
            ],
        },
        {
            viewtag: '6f8eeee8930cde732f5ea274a3ca1dbc',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbmxKHGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '321a2c02eb80f38bde111ba6ceeda15d',
            ],
        },
        {
            viewtag: '71d989522622828ecb9109e34f8d7af5',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbmxXZGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '17647fd91026d622fb7e3b49eeeea019',
            ],
        },
        {
            viewtag: '86536dfe782911ef08becd8d6bf5b175',
            viewmodule: 'PAR',
            viewname: 'ParExamContentGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'e7275e1243d582d3cb5fd9f5acb19adb',
                'c9e455b34eb6902a1c0a6fffffbf6dfe',
            ],
        },
        {
            viewtag: '9154733e9c78288a33d8342cd81eb5f3',
            viewmodule: 'PAR',
            viewname: 'ParExamCycleMPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '44b5e93634d7355506f37cde5dd7dde8',
            ],
        },
        {
            viewtag: '94611ef3a4827e95f4bea2199ed9b4a2',
            viewmodule: 'PAR',
            viewname: 'ParJxkhxhzYGKHXEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '963f9744f61222f20041022b8b7eba31',
            viewmodule: 'PAR',
            viewname: 'ParExamCycleEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '99f1bd86d47f8bfefef6803ebda3e437',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c763a9c626af79fb672c3156e55ee38d',
            ],
        },
        {
            viewtag: 'a0562928c523c5554de86ca7cce6cf14',
            viewmodule: 'PAR',
            viewname: 'ParKhfaEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'a97ed731f303a44f10abb4df79b7bfc5',
            viewmodule: 'PAR',
            viewname: 'ParJxjgKHDJEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'b7f7b92b88ba971f28bbb5aa18145e46',
            viewmodule: 'PAR',
            viewname: 'ParTzggEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'b8adaeafac9a1acdbd8f4aef296c22ee',
            viewmodule: 'PAR',
            viewname: 'ParJxjgGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '05888e4abd44fea4605d6f22c87eb807',
            ],
        },
        {
            viewtag: 'c088f921bed521fc40c770bfcf4b980e',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbNDLHMBXZEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '71d989522622828ecb9109e34f8d7af5',
                '64e14790f02145378336091f8f95421e',
            ],
        },
        {
            viewtag: 'C1ABBB2A-7CFE-453A-9531-C33DFC4D0912',
            viewmodule: 'PAR',
            viewname: 'Index',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '11beb7ce619d4fa3c5286eba91be6d7a',
                '188a0a8121edcb88a352a84cf562aa58',
                '51460be10bc6dc74ec79e53ae545e1c7',
                '11e69b35ff04251d7f8e7295a1d63f06',
                '4cb79515b9133499fc74b275a26e2da5',
                'b8adaeafac9a1acdbd8f4aef296c22ee',
                '2698c6c20aa3e029be5d7b71d05d529d',
                '2c011a5df68fea9e14fd6dd50a31b3da',
                '99f1bd86d47f8bfefef6803ebda3e437',
                '86536dfe782911ef08becd8d6bf5b175',
                '35977acfc0970b347cace0a7d8ccfac5',
                '25a6fb2dc9f85ee002bcbeca2f1a8205',
            ],
        },
        {
            viewtag: 'c254a4abcdeaa4de6c2969b4f4335e9e',
            viewmodule: 'PAR',
            viewname: 'ParKhzcmxEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '11e69b35ff04251d7f8e7295a1d63f06',
            ],
        },
        {
            viewtag: 'c763a9c626af79fb672c3156e55ee38d',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '6dfdcefce006ad3505558f3babcddcde',
                '64e14790f02145378336091f8f95421e',
            ],
        },
        {
            viewtag: 'c7741fa56792325b27e13d323a735061',
            viewmodule: 'PAR',
            viewname: 'ParTzggEditView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'b7f7b92b88ba971f28bbb5aa18145e46',
            ],
        },
        {
            viewtag: 'c9e455b34eb6902a1c0a6fffffbf6dfe',
            viewmodule: 'PAR',
            viewname: 'ParExamContentEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'ccbb9b1dc1b969f43ca9f0d6d6ad16b2',
            viewmodule: 'PIM',
            viewname: 'PimPersonPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'e519a0448aa58cc9ef26d17863520b2b',
            viewmodule: 'PAR',
            viewname: 'ParLdndlhmbKHEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '6f8eeee8930cde732f5ea274a3ca1dbc',
                '64e14790f02145378336091f8f95421e',
            ],
        },
        {
            viewtag: 'e7275e1243d582d3cb5fd9f5acb19adb',
            viewmodule: 'PAR',
            viewname: 'ParExamContentEditView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'e9be57ed7f3eaa058e17276a047e1d92',
            viewmodule: 'PAR',
            viewname: 'ParKhzcmxPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '0587c03dd8511394456567f75983d68d',
            ],
        },
    ],
    createdviews: [],
}